import time
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

dir_path = os.path.dirname(os.path.realpath(__file__))

def read_file_from_dir(filename):
	with open(dir_path + filename) as sites:
		return sites.readlines()

def convert_list_to_urls():
	lines = read_file_from_dir('/list.txt')
	urls = []
	for line in lines:
		a = line[1:-3]
		if '*' not in a:
			continue
		else:
			final_line = a[:-1]

		urls.append(final_line)
	return urls

def running():
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--disable-gpu')

	# ChromeDriver 83.0.4103.39
	browser = webdriver.Chrome('/usr/local/bin/chromedriver', options=options)
	urls = convert_list_to_urls()
	for url in urls:
		browser.get('https://' + url)
		browser.implicitly_wait(3)

		time.sleep(3)
		try:
			recall_link = browser.find_element_by_css_selector('a.cmpboxrecalllink')
			if recall_link is not None:
				recall_link.click()
				time.sleep(3)
		except:
			pass

		try:
			consent = browser.find_element_by_css_selector('a.cmpboxbtnyes')
			if consent is not None:
				consent.click()
				time.sleep(3)
		except:
			pass

		try:
			iam = browser.execute_script("return iam_data;")
			print(iam, url)
		except:
			print('None: https://' + url)
			pass


	browser.quit()




def simple_run():
	browser = webdriver.Chrome()
	browser.get('https://spielerplus.de')

	browser.implicitly_wait(3)

	time.sleep(3)
	try:
		recall_link = browser.find_element_by_css_selector('a.cmpboxrecalllink')
		if recall_link is not None:
			recall_link.click()
			time.sleep(3)
	except:
		pass

	try:
		consent = browser.find_element_by_css_selector('a.cmpboxbtnyes')
		if consent is not None:
			consent.click()
			time.sleep(3)
	except:
		pass

	print(browser.execute_script("return iam_data;"))

	browser.quit()


if __name__ == '__main__':
	running()